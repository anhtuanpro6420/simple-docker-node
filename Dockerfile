FROM node:13-alpine

WORKDIR /app

COPY . .

RUN npm config set package-lock false

RUN npm install

CMD ["npm", "start"]